<?php

namespace Oks\Bundle\AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('pow', 'pow'),
        );
    }
}
