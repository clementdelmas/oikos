<?php

namespace Oks\Bundle\AppBundle\Services\LoanCalculator;

use Oks\Bundle\AppBundle\Entity\Loan;

class LoanCalculator
{
    /**
     * Get a Loan entity with calculated variables
     *
     * @param Loan $loan - Loan entity to update
     *
     * @return Loan - Updated Loan entity
     */
    public function updateLoanWithCalculations(Loan $loan)
    {
        // Check number of payments
        if (!$loan->getNumberOfPayments()) {
            // Set total number of payments
            $loan->setNumberOfPayments(
                $loan->getDurationInYears() * $loan->getNumberOfPaymentsPerYear()
            );
        }

        // Calculate monthly payment
        $loan->setMonthlyPaymentDue(
            $this->calculatePaymentDue(
                $loan->getAnnualInterestRate(),
                $loan->getNumberOfPayments(),
                $loan->getTotal(),
                $loan->getNumberOfPaymentsPerYear()
            )
        );

        // Calculate loan payment
        $loan->calculateLoanPayments();

        // Calculate end date
        $loan->calculateEndDate();

        // Return updated loan
        return $loan;
    }

    /**
     * Calculate payment due.
     *
     * @param int $interest - Interest rate
     * @param int $months - Number of month
     * @param int $loan - Loan total
     * @param int $paymentsPerYear - Number of payments per year
     *
     * @return float - Payment due
     */
    public function calculatePaymentDue($interest, $months, $loan, $paymentsPerYear = 12)
    {
        $interest = $interest / (100 * $paymentsPerYear);
        $paymentDue = $interest * -$loan * pow(
            (1 + $interest),
            $months
        ) / (1 - pow((1 + $interest), $months))
        ;

        // Return payment due
        return $paymentDue;
    }
}
