<?php

namespace Oks\Bundle\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CalculatorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Initialize
        $defaultLoanStartDate = new \DateTime();
        $defaultLoanStartDate->modify('first day of next month');

        // Add form fields
        $builder->add('loan_total', NumberType::class, array(
            'label' => 'form.loan_total.label',
            'attr' => array(
                'placeholder' => 'form.loan_total.placeholder',
            ),
            'required' => true,
        ));
        $builder->add('annual_interest_rate', NumberType::class, array(
            'label' => 'form.annual_interest_rate.label',
            'attr' => array(
                'placeholder' => 'form.annual_interest_rate.placeholder',
            ),
            'required' => true,
        ));
        $builder->add('insurance_quota', ChoiceType::class, array(
            'label' => 'form.insurance_quota.label',
            'choices' => array(
                'form.insurance_quota.50_percent' => 50,
                'form.insurance_quota.75_percent' => 75,
                'form.insurance_quota.100_percent' => 100,
            ),
            'required' => true,
        ));
        $builder->add('insurance_rate', NumberType::class, array(
            'label' => 'form.insurance_rate.label',
            'attr' => array(
                'placeholder' => 'form.insurance_rate.placeholder',
            ),
            'required' => true,
        ));
        $builder->add('loan_duration', ChoiceType::class, array(
            'label' => 'form.loan_duration.label',
            'choices' => array(
                'form.loan_duration.10_years' => 10,
                'form.loan_duration.15_years' => 15,
                'form.loan_duration.20_years' => 20,
                'form.loan_duration.25_years' => 25,
                'form.loan_duration.30_years' => 30,
            ),
            'data' => 20,
            'required' => true,
        ));
        $builder->add('borrowers_number', ChoiceType::class, array(
            'label' => 'form.borrowers_number.label',
            'choices' => array(
                'form.borrowers_number.1_borrower' => 1,
                'form.borrowers_number.2_borrowers' => 2,
                'form.borrowers_number.3_borrowers' => 3,
                'form.borrowers_number.4_borrowers' => 4,
            ),
        ));
        $builder->add('loan_start_date', DateType::class, array(
            'label' => 'form.loan_start_date',
            'data' => $defaultLoanStartDate,
        ));
        $builder->add('submit_form', SubmitType::class, array(
            'label' => 'form.submit_form',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'calculator',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'calculator';
    }
}
