<?php

namespace Oks\Bundle\AppBundle\Entity;

use Oks\Bundle\AppBundle\Entity\City;
use Oks\Bundle\AppBundle\Entity\PropertyType;

/**
 * RealEstateAd.
 */
class RealEstateAd
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $cityId;

    /**
     * @var int
     */
    private $propertyTypeId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $price;

    /**
     * @var int
     */
    private $roomsNumber;

    /**
     * @var int
     */
    private $homeArea;

    /**
     * @var string
     */
    private $description;

    /**
     * @var bool
     */
    private $hasBeenCrawled;

    /**
     * @var int
     */
    private $totalPhotos;

    /**
     * @var bool
     */
    private $photosHaveBeenImported;

    /**
     * @var bool
     */
    private $isAgency;

    /**
     * @var string
     */
    private $agencyCompanyNumber;

    /**
     * @var bool
     */
    private $agencyFeesIncluded;

    /**
     * @var string
     */
    private $energy;

    /**
     * @var string
     */
    private $greenhouseGas;

    /**
     * @var string
     */
    private $websiteUrl;

    /**
     * @var string
     */
    private $websiteReference;

    /**
     * @var \DateTime
     */
    private $publishedAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var City
     */
    private $city;

    /**
     * @var PropertyType
     */
    private $propertyType;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cityId.
     *
     * @param int $cityId
     *
     * @return RealEstateAd
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * Get cityId.
     *
     * @return int
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set propertyTypeId.
     *
     * @param int $propertyTypeId
     *
     * @return RealEstateAd
     */
    public function setPropertyTypeId($propertyTypeId)
    {
        $this->propertyTypeId = $propertyTypeId;

        return $this;
    }

    /**
     * Get propertyTypeId.
     *
     * @return int
     */
    public function getPropertyTypeId()
    {
        return $this->propertyTypeId;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return RealEstateAd
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return RealEstateAd
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set roomsNumber.
     *
     * @param int $roomsNumber
     *
     * @return RealEstateAd
     */
    public function setRoomsNumber($roomsNumber)
    {
        $this->roomsNumber = $roomsNumber;

        return $this;
    }

    /**
     * Get roomsNumber.
     *
     * @return int
     */
    public function getRoomsNumber()
    {
        return $this->roomsNumber;
    }

    /**
     * Set homeArea.
     *
     * @param int $homeArea
     *
     * @return RealEstateAd
     */
    public function setHomeArea($homeArea)
    {
        $this->homeArea = $homeArea;

        return $this;
    }

    /**
     * Get homeArea.
     *
     * @return int
     */
    public function getHomeArea()
    {
        return $this->homeArea;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return RealEstateAd
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hasBeenCrawled.
     *
     * @param bool $hasBeenCrawled
     *
     * @return RealEstateAd
     */
    public function setHasBeenCrawled($hasBeenCrawled)
    {
        $this->hasBeenCrawled = $hasBeenCrawled;

        return $this;
    }

    /**
     * Get hasBeenCrawled.
     *
     * @return bool
     */
    public function hasBeenCrawled()
    {
        return $this->hasBeenCrawled;
    }

    /**
     * Set totalPhotos.
     *
     * @param int $totalPhotos
     *
     * @return RealEstateAd
     */
    public function setTotalPhotos($totalPhotos)
    {
        $this->totalPhotos = $totalPhotos;

        return $this;
    }

    /**
     * Get totalPhotos.
     *
     * @return int
     */
    public function getTotalPhotos()
    {
        return $this->totalPhotos;
    }

    /**
     * Set photosHaveBeenImported.
     *
     * @param bool $photosHaveBeenImported
     *
     * @return RealEstateAd
     */
    public function setPhotosHaveBeenImported($photosHaveBeenImported)
    {
        $this->photosHaveBeenImported = $photosHaveBeenImported;

        return $this;
    }

    /**
     * Get photosHaveBeenImported.
     *
     * @return bool
     */
    public function getPhotosHaveBeenImported()
    {
        return $this->photosHaveBeenImported;
    }

    /**
     * Set isAgency.
     *
     * @param bool $isAgency
     *
     * @return RealEstateAd
     */
    public function setAgency($isAgency)
    {
        $this->isAgency = $isAgency;

        return $this;
    }

    /**
     * Get isAgency.
     *
     * @return bool
     */
    public function isAgency()
    {
        return $this->isAgency;
    }

    /**
     * Set agencyCompanyNumber.
     *
     * @param string $agencyCompanyNumber
     *
     * @return RealEstateAd
     */
    public function setAgencyCompanyNumber($agencyCompanyNumber)
    {
        $this->agencyCompanyNumber = $agencyCompanyNumber;

        return $this;
    }

    /**
     * Get agencyCompanyNumber.
     *
     * @return string
     */
    public function getAgencyCompanyNumber()
    {
        return $this->agencyCompanyNumber;
    }

    /**
     * Set agencyFeesIncluded.
     *
     * @param bool $agencyFeesIncluded
     *
     * @return RealEstateAd
     */
    public function setAgencyFeesIncluded($agencyFeesIncluded)
    {
        $this->agencyFeesIncluded = $agencyFeesIncluded;

        return $this;
    }

    /**
     * Get agencyFeesIncluded.
     *
     * @return bool
     */
    public function getAgencyFeesIncluded()
    {
        return $this->agencyFeesIncluded;
    }

    /**
     * Set energy.
     *
     * @param string $energy
     *
     * @return RealEstateAd
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;

        return $this;
    }

    /**
     * Get energy.
     *
     * @return string
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set greenhouseGas.
     *
     * @param string $greenhouseGas
     *
     * @return RealEstateAd
     */
    public function setGreenhouseGas($greenhouseGas)
    {
        $this->greenhouseGas = $greenhouseGas;

        return $this;
    }

    /**
     * Get greenhouseGas.
     *
     * @return string
     */
    public function getGreenhouseGas()
    {
        return $this->greenhouseGas;
    }

    /**
     * Set websiteUrl.
     *
     * @param string $websiteUrl
     *
     * @return RealEstateAd
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * Get websiteUrl.
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * Set websiteReference.
     *
     * @param string $websiteReference
     *
     * @return RealEstateAd
     */
    public function setWebsiteReference($websiteReference)
    {
        $this->websiteReference = $websiteReference;

        return $this;
    }

    /**
     * Get websiteReference.
     *
     * @return string
     */
    public function getWebsiteReference()
    {
        return $this->websiteReference;
    }

    /**
     * Set publishedAt.
     *
     * @param \DateTime $publishedAt
     *
     * @return RealEstateAd
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt.
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return RealEstateAd
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return RealEstateAd
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set city.
     *
     * @param City $city
     *
     * @return RealEstateAd
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set propertyType.
     *
     * @param PropertyType $propertyType
     *
     * @return RealEstateAd
     */
    public function setPropertyType(PropertyType $propertyType = null)
    {
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * Get propertyType.
     *
     * @return PropertyType
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }
}
