<?php

namespace Oks\Bundle\AppBundle\Entity;

/**
 * City.
 */
class City
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $realEstateAds;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->realEstateAds = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set postalCode.
     *
     * @param string $postalCode
     *
     * @return City
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode.
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Add realEstateAd.
     *
     * @param \Oks\Bundle\AppBundle\Entity\RealEstateAd $realEstateAd
     *
     * @return City
     */
    public function addRealEstateAd(\Oks\Bundle\AppBundle\Entity\RealEstateAd $realEstateAd)
    {
        $this->realEstateAds[] = $realEstateAd;

        return $this;
    }

    /**
     * Remove realEstateAd.
     *
     * @param \Oks\Bundle\AppBundle\Entity\RealEstateAd $realEstateAd
     */
    public function removeRealEstateAd(\Oks\Bundle\AppBundle\Entity\RealEstateAd $realEstateAd)
    {
        $this->realEstateAds->removeElement($realEstateAd);
    }

    /**
     * Get realEstateAds.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealEstateAds()
    {
        return $this->realEstateAds;
    }

    /**
     * City as string.
     */
    public function __toString()
    {
        return $this->id ? $this->name.' ('.$this->postalCode.')' : 'New city';
    }
}
