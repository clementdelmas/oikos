<?php

namespace Oks\Bundle\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Oks\Bundle\AppBundle\Entity\RealEstateAd;

/**
 * PropertyType.
 */
class PropertyType
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $realEstateAds;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->realEstateAds = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PropertyType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return PropertyType
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add realEstateAd.
     *
     * @param RealEstateAd $realEstateAd
     *
     * @return PropertyType
     */
    public function addRealEstateAd(RealEstateAd $realEstateAd)
    {
        $this->realEstateAds[] = $realEstateAd;

        return $this;
    }

    /**
     * Remove realEstateAd.
     *
     * @param RealEstateAd $realEstateAd
     */
    public function removeRealEstateAd(RealEstateAd $realEstateAd)
    {
        $this->realEstateAds->removeElement($realEstateAd);
    }

    /**
     * Get realEstateAds.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealEstateAds()
    {
        return $this->realEstateAds;
    }
}
