<?php

namespace Oks\Bundle\AppBundle\Entity;

use \DateTime;

/**
 * Loan
 */
class Loan
{
    /**
     * Loan total.
     *
     * @var int
     */
    private $loanTotal;

    /**
     * Annual interest rate.
     *
     * @var float
     */
    private $annualInterestRate;

    /**
     * Duration of the loan in years.
     *
     * @var int
     */
    private $durationInYears;

    /**
     * Total number of payments
     *
     * @var int
     */
    private $numberOfPayments;

    /**
     * Number of payments per year
     * By default, it's 12 (as one payment per month).
     *
     * @var int
     */
    private $numberOfPaymentsPerYear = 12;

    /**
     * Monthly payment due.
     *
     * @var float
     */
    private $monthlyPaymentDue;

    /**
     * Total of monthly payments due.
     *
     * @var float
     */
    private $totalPaymentsDue;

    /**
     * Total of loan interest (bank part).
     *
     * @var int
     */
    private $totalLoanCost;

    /**
     * Insurance rate.
     *
     * @var float
     */
    private $insuranceRate;

    /**
     * Insurance quota per person (in percent)
     * By default, it's 100%.
     *
     * @var int
     */
    private $insuranceQuota = 100;

    /**
     * Monthly insurance payment.
     *
     * @var float
     */
    private $monthlyInsurancePayment;

    /**
     * Total insurance payment.
     *
     * @var float
     */
    private $totalInsurancePayment;

    /**
     * Total monthly due.
     *
     * @var float
     */
    private $totalMonthlyDue;

    /**
     * Loan start date.
     *
     * @var DateTime
     */
    private $startDate;

    /**
     * Loan end date.
     *
     * @var DateTime
     */
    private $endDate;

    /**
     * Number of borrowers.
     * By default, it's only one person.
     *
     * @var int
     */
    private $numberOfBorrowers = 1;

    /**
     * Get total.
     *
     * @return int Loan total
     */
    public function getTotal()
    {
        return $this->loanTotal;
    }

    /**
     * Set total.
     *
     * @param int Loan total
     */
    public function setTotal($loanTotal)
    {
        $this->loanTotal = $loanTotal;
    }

    /**
     * Get annual interest rate.
     *
     * @return float Annual interest rate
     */
    public function getAnnualInterestRate()
    {
        return $this->annualInterestRate;
    }

    /**
     * Set annual interest rate.
     *
     * @param float $annualInterestRate Annual interest rate
     */
    public function setAnnualInterestRate($annualInterestRate)
    {
        $this->annualInterestRate = $annualInterestRate;
    }

    /**
     * Get duration in years.
     *
     * @return int Duration in years
     */
    public function getDurationInYears()
    {
        return $this->durationInYears;
    }

    /**
     * Set duration in years.
     *
     * @param int $durationInYears Duration in years
     */
    public function setDurationInYears($durationInYears)
    {
        $this->durationInYears = $durationInYears;
    }

    /**
     * Get total number of payments.
     *
     * @return int Total number of payments
     */
    public function getNumberOfPayments()
    {
        // Check number of payments
        if ($this->numberOfPayments !== null || $this->numberOfPayments !== 0) {
            // Return number of payments
            return $this->numberOfPayments;
        }

        // Check durationInYears and numberOfPaymentsPerYear
        if ($this->durationInYears === null || $this->numberOfPaymentsPerYear === null) {
            return 0;
        }

        // Return number of payments
        $this->numberOfPayments = $this->durationInYears * $this->numberOfPaymentsPerYear;

        return $this->numberOfPayments;
    }

    /**
     * Set total number of payments.
     *
     * @param int $numberOfPayments - Number of payments
     */
    public function setNumberOfPayments($numberOfPayments)
    {
        $this->numberOfPayments = $numberOfPayments;
    }

    /**
     * Get number of payments per year.
     *
     * @return int Number of payments per year
     */
    public function getNumberOfPaymentsPerYear()
    {
        return $this->numberOfPaymentsPerYear;
    }

    /**
     * Set number of payments per year.
     *
     * @param int $numberOfPaymentsPerYear Number of payments per year
     */
    public function setNumberOfPaymentsPerYear($numberOfPaymentsPerYear)
    {
        $this->numberOfPaymentsPerYear = $numberOfPaymentsPerYear;
    }

    /**
     * Get monthly payment due.
     *
     * @return float Monthly payment due
     */
    public function getMonthlyPaymentDue()
    {
        return $this->monthlyPaymentDue;
    }

    /**
     * Set monthly payment due.
     *
     * @param float $monthlyPaymentDue Monthly payment due
     */
    public function setMonthlyPaymentDue($monthlyPaymentDue)
    {
        $this->monthlyPaymentDue = $monthlyPaymentDue;
    }

    /**
     * Get total payments due.
     *
     * @return float Total payments due
     */
    public function getTotalPaymentDue()
    {
        return $this->totalPaymentsDue;
    }

    /**
     * Set total payments due.
     *
     * @param float $totalPaymentsDue Total payments due
     */
    public function setTotalPaymentDue($totalPaymentsDue)
    {
        $this->totalPaymentsDue = $totalPaymentsDue;
    }

    /**
     * Get total loan cost.
     *
     * @return float Total loan cost
     */
    public function getTotalLoanCost()
    {
        return $this->totalLoanCost;
    }

    /**
     * Set total loan cost.
     *
     * @param float $totalLoanCost Total loan cost
     */
    public function setTotalLoanCost($totalLoanCost)
    {
        $this->totalLoanCost = $totalLoanCost;
    }

    /**
     * Get insurance quota.
     *
     * @return float Insurance quota
     */
    public function getInsuranceQuota()
    {
        return $this->insuranceQuota;
    }

    /**
     * Set insurance quota.
     *
     * @param float $insuranceQuota Insurance quota
     */
    public function setInsuranceQuota($insuranceQuota)
    {
        $this->insuranceQuota = $insuranceQuota;
    }

    /**
     * Get insurance rate.
     *
     * @return float Insurance Rate
     */
    public function getInsuranceRate()
    {
        return $this->insuranceRate;
    }

    /**
     * Set insurance rate.
     *
     * @param float $insuranceRate Insurance rate
     */
    public function setInsuranceRate($insuranceRate)
    {
        $this->insuranceRate = $insuranceRate;
    }

    /**
     * Get monthly insurance payment.
     *
     * @return float Monthly insurance payment
     */
    public function getMonthlyInsurancePayment()
    {
        return $this->monthlyInsurancePayment;
    }

    /**
     * Set monthly insurance payment.
     *
     * @param float $monthlyInsurancePayment Monthly insurance payment
     */
    public function setMonthlyInsurancePayment($monthlyInsurancePayment)
    {
        $this->monthlyInsurancePayment = $monthlyInsurancePayment;
    }

    /**
     * Get total insurance payment.
     *
     * @return float Total insurance payment
     */
    public function getTotalInsurancePayment()
    {
        return $this->totalInsurancePayment;
    }

    /**
     * Set total insurance payment.
     *
     * @param float $totalInsurancePayment Total insurance payment
     */
    public function setTotalInsurancePayment($totalInsurancePayment)
    {
        $this->totalInsurancePayment = $totalInsurancePayment;
    }

    /**
     * Get total monthly due.
     *
     * @return float Total monthly due
     */
    public function getTotalMonthlyDue()
    {
        return $this->totalMonthlyDue;
    }

    /**
     * Set total monthly due.
     *
     * @param float $totalMonthlyDue Total monthly due
     */
    public function setTotalMonthlyDue($totalMonthlyDue)
    {
        $this->totalMonthlyDue = $totalMonthlyDue;
    }

    /**
     * Get loan start date.
     *
     * @return DateTime Loan start date
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set loan start date.
     *
     * @param DateTime $startDate
     */
    public function setStartDate(DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Get loan end date.
     *
     * @return DateTime Loan end date
     */
    public function getEndDate()
    {
        // Check start date
        if (!($this->startDate instanceof DateTime)) {
            // Create start date
            $this->startDate = new DateTime();
        }

        // Set end date
        $this->calculateEndDate();

        // Return end date
        return $this->endDate;
    }

    /**
     * Set loan end date.
     *
     * @param DateTime $endDate
     */
    public function setEndDate(DateTime $endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * Get number of borrowers
     *
     * @return int Number of borrowers
     */
    public function getNumberOfBorrowers()
    {
        return $this->numberOfBorrowers;
    }

    /**
     * Set number of borrowers
     *
     * @param int $numberOfBorrowers
     */
    public function setNumberOfBorrowers($numberOfBorrowers)
    {
        $this->numberOfBorrowers = $numberOfBorrowers;
    }

    /**
     * Initialize Loan with form data
     *
     * @param int $loanTotal - Loan total
     * @param float $annualInterestRate - Annual interest rate
     * @param int $durationInYears - Loan duration in years
     * @param int $insuranceRate - Insurance rate
     *
     * @return Loan - Initialized loan
     */
    public static function initializeWithFormData($loanTotal, $annualInterestRate, $durationInYears, $insuranceRate = 0)
    {
        // Initialize
        $loan = new self();

        // Set form data
        $loan->loanTotal          = $loanTotal;
        $loan->annualInterestRate = $annualInterestRate;
        $loan->durationInYears    = $durationInYears;
        $loan->insuranceRate      = $insuranceRate;

        // Return initialized loan
        return $loan;
    }

    /**
     * Get insurance quota divider.
     *
     * @return float Insurance quota divider
     */
    public function getInsuranceQuotaDivider()
    {
        return (
            100 /
            ($this->getInsuranceQuota() * $this->numberOfBorrowers / 100)
        );
    }

    /**
     * Calculate end date
     *
     * @return void
     */
    public function calculateEndDate()
    {
        $this->endDate = clone $this->startDate;
        $this->endDate->modify('+'.$this->durationInYears.' years');
    }

    /**
     * Get total insurance quota
     *
     * @return int Total insurance quota
     */
    public function getTotalInsuranceQuota()
    {
        return $this->insuranceQuota * $this->numberOfBorrowers;
    }

    /**
     * Calculate loan payments.
     *
     * @return void
     */
    public function calculateLoanPayments()
    {
        // Initialize
        $insuranceQuotaDivider = $this->getInsuranceQuotaDivider();

        // Calculate insurance payment and total
        $this->monthlyInsurancePayment = (
            $this->loanTotal *
            $this->insuranceRate /
            $insuranceQuotaDivider /
            $this->numberOfPaymentsPerYear
        );
        $this->totalInsurancePayment = (
            $this->monthlyInsurancePayment *
            $this->numberOfPaymentsPerYear *
            $this->durationInYears
        );

        // Calculate payments total
        $this->totalPaymentsDue = (
            $this->monthlyPaymentDue * $this->getNumberOfPayments()
        );

        // Calculate loan total cost
        $this->totalMonthlyDue = (
            $this->monthlyPaymentDue +
            $this->monthlyInsurancePayment
        );
        $this->totalLoanCost = (
            $this->totalPaymentsDue - $this->loanTotal
        );
    }

    public function getMonthlyPaymentDueWithoutInterest()
    {

    }
}
