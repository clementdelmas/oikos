<?php

namespace Oks\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LayoutController extends Controller
{
    /**
     * Sidebar links.
     */
    public function sidebarLinksAction(Request $request)
    {
        // Initialize
        $currentRoute = $request->get('route');

        // Initialize links
        $links = array(
            array(
                'class' => ($currentRoute === 'oks_app_dashboard') ? 'active' : '',
                'route' => 'oks_app_dashboard',
                'icon'  => 'ti-pie-chart',
                'title' => 'Layout.Menu.Dashboard',
            ),
            array(
                'class' => ($currentRoute === 'oks_app_calculator') ? 'active' : '',
                'route' => 'oks_app_calculator',
                'icon'  => 'ti-money',
                'title' => 'Layout.Menu.Calculator',
            ),
        );

        return $this->render('OksAppBundle:Layout:sidebar-links.html.twig', array(
            'links' => $links,
        ));
    }
}
