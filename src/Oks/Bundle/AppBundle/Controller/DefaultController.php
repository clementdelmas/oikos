<?php

namespace Oks\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * Homepage.
     */
    public function indexAction()
    {
        // Initialize
        $em = $this->getDoctrine()->getManager();

        // Get last real estate ads
        $lastRealEstateAds = $em->getRepository('OksAppBundle:RealEstateAd')->findBy(
            array(),
            array('updatedAt' => 'DESC'),
            10
        );

        return $this->render('OksAppBundle:Home:index.html.twig', array(
            'last_real_estate_ads' => $lastRealEstateAds,
        ));
    }
}
