<?php

namespace Oks\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Oks\Bundle\AppBundle\Entity\Loan;
use Oks\Bundle\AppBundle\Form\Type\CalculatorType;
use Symfony\Component\HttpFoundation\Response;

class CalculatorController extends Controller
{
    /**
     * Calculator.
     *
     * @param Request $request - Main request
     *
     * @return Response - Form view
     */
    public function indexAction(Request $request)
    {
        // Initialize
        $loan = false;
        $formData = array();
        $calculatorForm = $this->createForm(CalculatorType::class, $formData);

        // Check form
        if ($request->getMethod() === 'POST') {
            // Bind form
            $calculatorForm->handleRequest($request);

            // Check form
            if ($calculatorForm->isValid()) {
                // Do calculate loan
                $loan = Loan::initializeWithFormData(
                    $calculatorForm->get('loan_total')->getData(),
                    $calculatorForm->get('annual_interest_rate')->getData(),
                    $calculatorForm->get('loan_duration')->getData(),
                    $calculatorForm->get('insurance_rate')->getData()
                );
                $loan->setNumberOfBorrowers($calculatorForm->get('borrowers_number')->getData());
                $loan->setInsuranceQuota($calculatorForm->get('insurance_quota')->getData());
                $loan->setStartDate($calculatorForm->get('loan_start_date')->getData());

                // Do calculate loan payments
                $loan = $this->container->get('oks_app.services.loan_calculator')
                                        ->updateLoanWithCalculations($loan);
            }
        }

        // Render template
        return $this->render('OksAppBundle:Calculator:index.html.twig', array(
            'calculator_form' => $calculatorForm->createView(),
            'loan' => $loan,
        ));
    }
}
