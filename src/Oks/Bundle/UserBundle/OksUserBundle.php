<?php

namespace Oks\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OksUserBundle extends Bundle
{
    /**
     * @{inheritDoc}
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
