<?php

namespace Oks\Bundle\CrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *  Crawl Real Estate Ad.
 */
class CrawlRealEstateAdCommand extends ContainerAwareCommand
{
    /**
     *  Main configure action.
     *
     *  @return (void)
     */
    protected function configure()
    {
        $this
            ->setName('oks:crawler:crawl')
            ->setDescription('Crawl a given real estate ad and add it to the database')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'Url of the real estate ad to crawl'
            )
        ;
    }

    /**
     *  Main execute action.
     *
     *  @param (InputInterface) $input
     *  @param (OutputInterface) $output
     *
     *  @return (void)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Initialize
        $io = new SymfonyStyle($input, $output);
        $urlToCrawl = $input->getArgument('url');

        // Check url
        if (!filter_var($urlToCrawl, FILTER_VALIDATE_URL)) {
            $io->error('Given url is invalid!');

            return false;
        }

        // Get crawler
        $crawler = $this->getContainer()->get('oks_crawler.realestateadcrawler.factory')->getCrawlerByUrl($urlToCrawl);
        $crawledRealEstateAd = $crawler->crawl($urlToCrawl);

        // Do create/update real estate ad
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($crawledRealEstateAd);
        $em->flush();

        // This is a huge success!
        $io->success('RealEstateAd has been crawled and imported into the database');
    }
}
