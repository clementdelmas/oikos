<?php

namespace Oks\Bundle\CrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *  Crawl Real Estate Ads list.
 */
class CrawlRealEstateAdsListCommand extends ContainerAwareCommand
{
    /**
     *  Main configure action.
     *
     *  @return (void)
     */
    protected function configure()
    {
        $this
            ->setName('oks:crawler:crawl-list')
            ->setDescription('Crawl a given real estate ads list')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'Url of the real estate ads list to crawl'
            )
        ;
    }

    /**
     *  Main execute action.
     *
     *  @param (InputInterface) $input
     *  @param (OutputInterface) $output
     *
     *  @return (void)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Initialize
        $io = new SymfonyStyle($input, $output);
        $urlToCrawl = $input->getArgument('url');

        // Check url
        if (!filter_var($urlToCrawl, FILTER_VALIDATE_URL)) {
            $io->error('Given url is invalid!');

            return false;
        }

        // Get crawler
        $crawler = $this->getContainer()->get('oks_crawler.realestateadcrawler.factory')->getCrawlerByUrl($urlToCrawl);
        $crawledRealEstateAdsList = $crawler->crawlList($urlToCrawl);

        // Check list
        if (count($crawledRealEstateAdsList) < 1) {
            // No ads founds
            $io->error('No ads found!');

            return false;
        }

        // Initialize
        $em = $this->getContainer()->get('doctrine')->getManager();

        // Loop on crawled ads
        foreach ($crawledRealEstateAdsList as $crawledRealEstateAd) {
            // Do create/update real estate ad
            $em->persist($crawledRealEstateAd);
        }

        // Do flush
        $em->flush();

        // This is a huge success!
        $io->success(count($crawledRealEstateAdsList).' have been crawled and imported into the database!');
    }
}
