<?php

namespace Oks\Bundle\CrawlerBundle\Crawlers\RealEstateAd;

use Symfony\Component\DomCrawler\Crawler;
use Oks\Bundle\CrawlerBundle\Crawlers\RealEstateAd\LeBonCoin\SingleAdPageTrait;
use Oks\Bundle\CrawlerBundle\Crawlers\RealEstateAd\LeBonCoin\AdsListPageTrait;

/**
 * Crawler for the french website 'LeBonCoin.fr'.
 */
class LeBonCoinCrawler extends RealEstateAdCrawler implements RealEstateAdCrawlerInterface
{
    use SingleAdPageTrait;
    use AdsListPageTrait;
}
