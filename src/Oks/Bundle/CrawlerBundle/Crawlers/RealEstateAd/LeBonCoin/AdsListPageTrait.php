<?php

namespace Oks\Bundle\CrawlerBundle\Crawlers\RealEstateAd\LeBonCoin;

use Symfony\Component\DomCrawler\Crawler;
use Oks\Bundle\AppBundle\Entity\RealEstateAd;

/**
 * All methods for an ads list page.
 *
 * @see This is an ads list page
 * - https://www.leboncoin.fr/ventes_immobilieres/offres/ile_de_france/?ps=10&pe=15&sqs=9&ret=1&location=Torcy+77200&f=p
 */
trait AdsListPageTrait
{
    /**
     * {@inheritdoc}
     */
    public function crawlList($listUrlToCrawl)
    {
        // Initialize
        $realEstateAds = new \StdClass();
        $realEstateAds->list = array();
        $crawler = $this->goutteClient->request('GET', $listUrlToCrawl);

        // Loop on list
        // @warning: We have to use an object here because a simple array doesn't work!
        $crawler->filter('.tabsContent li')->each(function (Crawler $node) use ($realEstateAds) {
            // Initialize: do add real estate ad to list
            $realEstateAd = $this->getRealEstateAdFromListNode($node);
            $realEstateAds->list[] = $realEstateAd;
        });

        // Get real estate ads list
        return $realEstateAds->list;
    }

    /**
     * Get real estate ad from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (RealEstateAd) Found real estate ad
     */
    private function getRealEstateAdFromListNode(Crawler $crawler)
    {
        // Initialize
        $normalizedInformation = $this->getNormalizedInformationFromListNode($crawler);
        $realEstateAd = $this->getRealEstateAdByWebsiteUrl($normalizedInformation['websiteUrl']);

        // Check if entity alreay exists
        if (!$realEstateAd) {
            // Create new entity
            $realEstateAd = new RealEstateAd();
        }

        // Check if entity has been crawled
        if ($realEstateAd->hasBeenCrawled()) {
            // Don't update real estate ad
            return $realEstateAd;
        }

        // Update entity with information
        $realEstateAd->setCity($normalizedInformation['city']);
        $realEstateAd->setTitle($normalizedInformation['title']);
        $realEstateAd->setPrice($normalizedInformation['price']);
        $realEstateAd->setTotalPhotos($normalizedInformation['totalPhotos']);
        $realEstateAd->setWebsiteUrl($normalizedInformation['websiteUrl']);
        $realEstateAd->setWebsiteReference($normalizedInformation['websiteReference']);
        $realEstateAd->setHasBeenCrawled(false);
        $realEstateAd->setPhotosHaveBeenImported(false);
        $realEstateAd->setAgency($normalizedInformation['isAgency']);

        // Return updated entity
        return $realEstateAd;
    }

    /**
     * Get normalized information from list.
     *
     * @param (Crawler) $crawler
     *
     * @return (RealEstateAd) RealEstateAd entity
     */
    private function getNormalizedInformationFromListNode(Crawler $crawler)
    {
        return array(
            'city' => $this->getCityFromListNode($crawler),
            'title' => $this->getTitleFromListNode($crawler),
            'price' => $this->getPriceFromListNode($crawler),
            'totalPhotos' => $this->getTotalPhotosFromListNode($crawler),
            'websiteUrl' => $this->getWebsiteUrlFromListNode($crawler),
            'websiteReference' => $this->getWebsiteReferenceFromListNode($crawler),
            'isAgency' => $this->getAgencyStatusFromListNode($crawler),
        );
    }

    /**
     * Get city from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (City) City entity
     */
    private function getCityFromListNode(Crawler $crawler)
    {
        // Initialize
        $cityNode = $crawler->filter('p.item_supp')->eq(1);
        $fullCity = trim($cityNode->text());
        preg_match('/(.*)\s+\/\s+(.*)/', $fullCity, $cityInfo);

        // Try to get city
        $city = $this->getCityByName($cityInfo[1]);

        // Check city
        if (!$city) {
            // Throw error
            throw new \Exception("Can't find ad's city", 1);
        }

        // Return city
        return $city;
    }

    /**
     * Get title from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (string) Real estate ad's title
     */
    private function getTitleFromListNode(Crawler $crawler)
    {
        // Initialize
        $mainLink = $crawler->filter('a')->first();

        // Return title
        return $mainLink->attr('title');
    }

    /**
     * Get price from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (int) Real estate ad's price
     */
    private function getPriceFromListNode(Crawler $crawler)
    {
        // Initialize
        $price = $crawler->filter('.item_price')->first()->text();

        // Return price
        return intval(preg_replace('/[^0-9]/', '', $price));
    }

    /**
     * Get number of photos from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (int) Real estate ad's number of photos
     */
    private function getTotalPhotosFromListNode(Crawler $crawler)
    {
        // Try to get number of photos
        try {
            // Initialize
            $photos = $crawler->filter('.item_imageNumber')->first()->text();

            // Return number of photos
            return intval(trim($photos));
        } catch (\Exception $e) {
            // Can't find number of photos (the 'current node list' is probably empty)
            return 0;
        }
    }

    /**
     * Get website's url from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (int) Real estate ad's url
     */
    private function getWebsiteUrlFromListNode(Crawler $crawler)
    {
        // Initialize
        $mainLink = $crawler->filter('a')->first()->attr('href');

        // Check main link
        if (strpos($mainLink, '//') === 0) {
            // Do append protocol
            $mainLink = 'https:'.$mainLink;
        }

        // Return url
        return str_replace('?ca=12_s', '', $mainLink);
    }

    /**
     * Get number of photos from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (int) Real estate ad's number of photos
     */
    private function getWebsiteReferenceFromListNode(Crawler $crawler)
    {
        // Initialize
        $mainLink = $crawler->filter('a')->first();
        $mainLinkDataInfo = $mainLink->attr('data-info');
        $dataInfo = json_decode($mainLinkDataInfo);

        // Check data info
        if (isset($dataInfo->ad_listid) && !empty($dataInfo->ad_listid)) {
            // Return website reference
            return $dataInfo->ad_listid;
        }

        // Initialize
        $urlRegex = '/.*leboncoin\.fr\/[^\/]*\/([0-9]*)\.htm/';
        preg_match($urlRegex, $mainLink->attr('href'), $mainLinkDataInfo);

        // Checl main link data infos
        if (count($mainLinkDataInfo) == 2) {
            // Return found reference
            return $mainLinkDataInfo[1];
        }

        // Throw error
        throw new \Exception("Can't get Website reference!", 1);
    }

    /**
     * Get agency status from list node.
     *
     * @param (Crawler) $crawler
     *
     * @return (boolean) Real estate ad's agency status
     */
    private function getAgencyStatusFromListNode(Crawler $crawler)
    {
        // Initialize
        $agencyContainer = $crawler->filter('.ispro');

        // Return status
        return $agencyContainer->count() > 0;
    }
}
