<?php

namespace Oks\Bundle\CrawlerBundle\Crawlers\RealEstateAd\LeBonCoin;

use Symfony\Component\DomCrawler\Crawler;
use Oks\Bundle\AppBundle\Entity\RealEstateAd;

/**
 * All methods for a single ad page.
 *
 * @see This is a single ad page
 * - https://www.leboncoin.fr/ventes_immobilieres/951778264.htm?ca=12_s
 */
trait SingleAdPageTrait
{
    /**
     * {@inheritdoc}
     */
    public function crawl($urlToCrawl)
    {
        // Initialize
        $crawler = $this->goutteClient->request('GET', $urlToCrawl);

        // Get normalized information
        $normalizedInformation = $this->getNormalizedInformationOfSingleAdPage($crawler);

        // Check if entity already exists
        $realEstateAd = $this->em->getRepository('OksAppBundle:RealEstateAd')
                                    ->findOneByWebsiteReference($normalizedInformation['websiteReference']);
        if (!$realEstateAd) {
            // Create new entity
            $realEstateAd = new RealEstateAd();
        }

        // Do clean url to crawl
        $urlToCrawl = str_replace('?ca=12_s', '', $urlToCrawl);

        // Update real estate ad properties
        $realEstateAd->setCity($normalizedInformation['city']);
        $realEstateAd->setPropertyType($normalizedInformation['propertyType']);
        $realEstateAd->setTitle($normalizedInformation['title']);
        $realEstateAd->setPrice($normalizedInformation['price']);
        $realEstateAd->setRoomsNumber($normalizedInformation['roomsNumber']);
        $realEstateAd->setHomeArea($normalizedInformation['homeArea']);
        $realEstateAd->setDescription($normalizedInformation['description']);
        $realEstateAd->setEnergy($normalizedInformation['energy']);
        $realEstateAd->setGreenhouseGas($normalizedInformation['greenhouseGas']);
        $realEstateAd->setWebsiteUrl($urlToCrawl);
        $realEstateAd->setWebsiteReference($normalizedInformation['websiteReference']);
        $realEstateAd->setPublishedAt($normalizedInformation['publishedAt']);
        $realEstateAd->setHasBeenCrawled(true);
        $realEstateAd->setTotalPhotos($normalizedInformation['totalPhotos']);
        $realEstateAd->setPhotosHaveBeenImported(false);
        $realEstateAd->setAgency($normalizedInformation['isAgency']);
        $realEstateAd->setAgencyCompanyNumber($normalizedInformation['agencyCompanyNumber']);
        $realEstateAd->setAgencyFeesIncluded($normalizedInformation['agencyFeesIncluded']);

        // Return real estate ad
        return $realEstateAd;
    }

    /**
     * Get normalized information of a given DOMCrawler of a single real estate ad page.
     *
     * @param (Crawler) $crawler Crawler
     *
     * @return (array) Normalized information
     */
    private function getNormalizedInformationOfSingleAdPage(Crawler $crawler)
    {
        // Initialize
        $tagData = $this->getDataInScriptTagsOfSingleAdPage($crawler);

        // Return found and normalized information
        return array(
            'city' => $this->getCityOfSingleAdPage($crawler),
            'title' => $this->getTitleOfSingleAdPage($crawler),
            'propertyType' => $this->getPropertyTypeOfSingleAdPage($tagData['type']),
            'price' => intval($tagData['prix']),
            'roomsNumber' => intval($tagData['pieces']),
            'totalPhotos' => intval($tagData['nbphoto']),
            'homeArea' => intval($tagData['surface']),
            'description' => $this->getDescriptionOfSingleAdPage($crawler),
            'energy' => isset($tagData['nrj']) ? strtoupper($tagData['nrj']) : '',
            'greenhouseGas' => isset($tagData['ges']) ? strtoupper($tagData['ges']) : '',
            'websiteReference' => $tagData['listid'],
            'publishedAt' => new \DateTime($tagData['publish_date']),
            'isAgency' => isset($tagData['siren']),
            'agencyCompanyNumber' => isset($tagData['siren']) ? $tagData['siren'] : '',
            'agencyFeesIncluded' => $this->checkIfAgencyFeesAreIncluded($crawler),
        );
    }

    /**
     * Get City of crawled city.
     *
     * @param (Crawler) $crawler
     *
     * @return (City) City
     */
    private function getCityOfSingleAdPage(Crawler $crawler)
    {
        // Initialize
        $cityNode = $crawler->filter('span[itemtype="http://schema.org/PostalAddress"]');
        $fullCity = trim($cityNode->html());
        preg_match('/(.*) (\d+)/', $fullCity, $cityInfo);
        list($fullCity, $cityName, $cityPostalCode) = $cityInfo;

        // Try to get city
        $city = $this->getCityByPostalCode($cityPostalCode);

        // Check city
        if (!$city) {
            // We need to create city
            $city = $this->addAndGetCity(array(
                'name' => $cityName,
                'postal_code' => $cityPostalCode,
            ));
        }

        // Return city
        return $city;
    }

    /**
     * Get PropertyType of crawled property type.
     *
     * @param (Crawler) $crawler
     *
     * @return (int) PropertyType's id
     */
    private function getPropertyTypeOfSingleAdPage($propertyTypeSlug)
    {
        // Initialize
        $propertyType = $this->getPropertyTypeBySlug($propertyTypeSlug);

        // Check property type
        if (!$propertyType) {
            // Throw error
            throw new \InvalidArgumentException();
        }

        // Return property type
        return $propertyType;
    }

    /**
     * Get title.
     *
     * @param (Crawler) $crawler
     *
     * @return (string) Title
     */
    private function getTitleOfSingleAdPage(Crawler $crawler)
    {
        // Initialize
        $title = $crawler->filter('title')->html();

        // Clean title
        $cleanedTitle = preg_replace('/(.*) Ventes immo.*/', '$1', $title);

        // Return cleaned title
        return $cleanedTitle;
    }

    /**
     * Get description.
     *
     * @param (Crawler) $crawler
     *
     * @return (string) Description
     */
    private function getDescriptionOfSingleAdPage(Crawler $crawler)
    {
        // Initialize
        $description = $crawler->filter('p[itemprop="description"]')->html();

        // Return formatted description
        return str_replace('<br>', "\n", $description);
    }

    /**
     * Get data in script tags.
     *
     * @param (Crawler) $crawler
     *
     * @return (array) Crawled data
     */
    private function getDataInScriptTagsOfSingleAdPage(Crawler $crawler)
    {
        // Initialize : get tag data script
        $tagDataScript = $crawler->filter('body > script')->reduce(function (Crawler $node) {
            return strpos($node->html(), 'utag_data') !== false;
        });

        // Format tag data script
        $tagDataContent = str_replace('var utag_data = {', '', trim($tagDataScript->first()->html()));
        $tagDataContent = trim($tagDataContent);

        // Filter unwanted lines
        $data = array();
        $wantedProperties = array(
            'listid', 'siren', 'publish_date', 'nbphoto', 'prix', 'surface',
            'pieces', 'type', 'ges', 'nrj', 'siren',
        );
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $tagDataContent) as $line) {
            // Initialize: check property
            if (in_array(strtok($line, ' : '), $wantedProperties)) {
                // Do keep line
                preg_match('/([^\s]*) \: \"?([^\"\,]*)\"?\,?/', $line, $lineData);
                $data[$lineData[1]] = $lineData[2];
            }
        }

        // Check data
        if (empty($data)) {
            // Throw error
            throw new \Exception('Error processing data in script tags', 1);
        }

        // Format published date
        // Yes, this is a bit ugly but if you have a better solution,
        // I'll be open to use it :)
        $tmpPublishDate = explode('/', $data['publish_date']);
        $data['publish_date'] = $tmpPublishDate[2].'-'.$tmpPublishDate[1].'-'.$tmpPublishDate[0];

        // Return data
        return $data;
    }

    /**
     * Check if agency fees are included.
     *
     * @param (Crawler) $crawler
     *
     * @return (boolean) True if agency fees are included
     */
    private function checkIfAgencyFeesAreIncluded(Crawler $crawler)
    {
        // Initialize
        $baseAgencyFeesText = "Frais d'agence inclus";
        $agencyFeesContainer = $crawler->filter('section.properties .line')
                                        ->reduce(function (Crawler $node) use ($baseAgencyFeesText) {
                                            return strpos($node->text(), $baseAgencyFeesText) !== false;
                                        })
        ;

        // Check agency fees container
        if (!($agencyFeesContainer instanceof Crawler) || $agencyFeesContainer->count() === 0) {
            // Can't find text
            return false;
        }

        // Check agency fees
        $agencyFeesText = trim(str_replace($baseAgencyFeesText, '', $agencyFeesContainer->text()));

        // Return status
        return strtolower($agencyFeesText) == 'oui';
    }
}
