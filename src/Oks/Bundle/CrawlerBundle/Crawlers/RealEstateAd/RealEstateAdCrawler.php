<?php

namespace Oks\Bundle\CrawlerBundle\Crawlers\RealEstateAd;

use Doctrine\ORM\EntityManager;
use Goutte\Client as GoutteClient;
use Oks\Bundle\AppBundle\Entity\City;

/**
 * Main real estate ad crawler.
 */
class RealEstateAdCrawler
{
    /**
     * @var GoutteClient
     */
    protected $goutteClient;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var DoctrineCollection
     */
    protected $cities;

    /**
     * @var DoctrineCollection
     */
    protected $propertyTypes;

    /**
     * @var DocrtrineCollection
     */
    protected $realEstateAds;

    /**
     * Main constructor.
     */
    public function __construct(GoutteClient $goutteClient, EntityManager $em)
    {
        // Initialize
        $this->goutteClient = $goutteClient;
        $this->em = $em;

        // Get all cities and property types
        $this->cities = $this->em->getRepository('OksAppBundle:City')->findAll();
        $this->propertyTypes = $this->em->getRepository('OksAppBundle:PropertyType')->findAll();
        $this->realEstateAds = $this->em->getRepository('OksAppBundle:RealEstateAd')->findAll();
    }

    /**
     * Get city by postal code.
     *
     * @param (string) $postalCode Postal code
     *
     * @return (mixed) Found city or false on error
     */
    protected function getCityByPostalCode($postalCode)
    {
        // Initialize
        $foundCity = false;

        // Loop on cities
        foreach ($this->cities as $city) {
            // Check city
            if ($city->getPostalCode() == $postalCode) {
                // We found it!
                $foundCity = $city;
                break;
            }
        }

        // Return found city
        return $foundCity;
    }

    /**
     * Get city by name.
     *
     * @param (string) $cityName City name
     *
     * @return (mixed) Found city or false on error
     */
    protected function getCityByName($cityName)
    {
        // Initialize
        $foundCity = false;

        // Loop on cities
        foreach ($this->cities as $city) {
            // Check city
            if ($city->getName() == $cityName) {
                // We found it!
                $foundCity = $city;
                break;
            }
        }

        // Return found city
        return $foundCity;
    }

    /**
     * Add city to cities list and retrieve created city entity.
     *
     * @param (array) $cityAsArray City as array (with postal_code and name keys)
     *
     * @return (City) Created city entity
     */
    protected function addAndGetCity(array $cityAsArray)
    {
        // Initialize
        if (!array_key_exists('name', $cityAsArray) || !array_key_exists('postal_code', $cityAsArray)) {
            // Throw error
            throw new \InvalidArgumentException();
        }

        // Create city
        $city = new City();
        $city->setName($cityAsArray['name']);
        $city->setPostalCode($cityAsArray['postal_code']);
        $this->em->persist($city);
        $this->em->flush();

        // Add city to cities list
        $this->cities[] = $city;

        // Return added city
        return $city;
    }

    /**
     * Get property type by slug.
     *
     * @param (string) $propertyTypeSlug Property type's slug
     *
     * @return (mixed) Found property type or false on error
     */
    protected function getPropertyTypeBySlug($propertyTypeSlug)
    {
        // Initialize
        $foundPropertyType = false;

        // Loop on propertyTypes
        foreach ($this->propertyTypes as $propertyType) {
            // Check property type
            if ($propertyType->getSlug() == $propertyTypeSlug) {
                // We found it!
                $foundPropertyType = $propertyType;
                break;
            }
        }

        // Return found city
        return $foundPropertyType;
    }

    /**
     * Get real estate ad by website url.
     *
     * @param (string) $websiteUrl RealEstateAd's website url
     *
     * @return (mixed) Found real estate ad or false on error
     */
    protected function getRealEstateAdByWebsiteUrl($realEstateAdWebsiteUrl)
    {
        // Initialize
        $foundRealEstateAd = false;

        // Loop on propertyTypes
        foreach ($this->realEstateAds as $realEstateAd) {
            // Check real estate ad
            if ($realEstateAd->getWebsiteUrl() == $realEstateAdWebsiteUrl) {
                // We found it!
                $foundRealEstateAd = $realEstateAd;
                break;
            }
        }

        // Return found city
        return $foundRealEstateAd;
    }
}
