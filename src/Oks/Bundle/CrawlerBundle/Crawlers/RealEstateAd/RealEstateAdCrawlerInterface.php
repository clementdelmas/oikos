<?php

namespace Oks\Bundle\CrawlerBundle\Crawlers\RealEstateAd;

/**
 * Main interface for crawlers.
 */
interface RealEstateAdCrawlerInterface
{
    /**
     * Crawl a given url and return a list of RealEstateAd entities.
     *
     * @param (string) $listUrlToCrawl Url to crawl
     *
     * @return (Array) List of RealEstateAd entities
     */
    public function crawlList($listUrlToCrawl);

    /**
     * Crawl a given url and return RealEstateAd entity.
     *
     * @param (string) $urlToCrawl Url to crawl
     *
     * @return (RealEstateAd) RealEstateAd entity
     */
    public function crawl($urlToCrawl);
}
