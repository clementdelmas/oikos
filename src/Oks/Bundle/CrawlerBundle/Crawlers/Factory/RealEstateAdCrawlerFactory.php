<?php

namespace Oks\Bundle\CrawlerBundle\Crawlers\Factory;

/**
 * Real estate ad crawlers' "factory".
 */
class RealEstateAdCrawlerFactory
{
    /**
     * @var (appDevDebugProjectContainer)
     */
    private $serviceContainer;

    /**
     * Main constructor.
     *
     * @param (appDevDebugProjectContainer) $serviceContainer Service container
     */
    public function __construct($serviceContainer)
    {
        // Initialize
        $this->serviceContainer = $serviceContainer;
    }

    /**
     * Get real estate ad crawler.
     *
     * @return (RealEstateAdCrawlerInterface) Crawler
     */
    public function getCrawlerByUrl($urlToCrawl)
    {
        // Initialize
        $crawler = null;
        $urlHost = parse_url($urlToCrawl, PHP_URL_HOST);

        // Check url
        switch ($urlHost) {
            case 'www.leboncoin.fr':
                $crawler = $this->serviceContainer->get('oks_crawler.realestatead.leboncoin');
                break;

            default:
                throw new \InvalidArgumentException();
        }

        // Return crawler
        return $crawler;
    }
}
