<?php

namespace Oks\Bundle\CrawlerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OksCrawlerBundle:Default:index.html.twig');
    }
}
