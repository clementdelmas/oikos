<?php

namespace Oks\Component\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * A web test case with included translator service
 *
 * @see http://www.remy-mellet.com/blog/315-create-test-symfony-service/
 */
class WebTestCaseWithTranslator extends WebTestCase
{
    /**
     * Translator service
     *
     * @var Symfony\Component\Translation\DataCollectorTranslator
     */
    protected static $translator;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        self::$translator = $kernel->getContainer()->get('translator');
    }
}
