# Oikos

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/79d1bbd0-d382-4a71-9973-b30566b9521a/mini.png)](https://insight.sensiolabs.com/projects/79d1bbd0-d382-4a71-9973-b30566b9521a) [![CodeShip](https://codeship.com/projects/652159c0-239e-0134-af3e-0a91dc871829/status?branch=master)](https://codeship.com/projects/652159c0-239e-0134-af3e-0a91dc871829/status?branch=master)

This project is a side-project about real estate ads and properties.
It is not in development anymore and is not really usable for now.

## How to use it

For the moment, we only have two features:

- a list of real estate ads in the database
- and a loan simulator

### Real estate ads

This feature only has a list view in the app, but has a built-in crawler to get real estate ads.
For the moment, the only website supported is *LeBonCoin* (a french one).

Here is how you can crawl real estate ads :

- `oks:crawler:crawl [real-estate-ad-url]`
- `oks:crawler:crawl-list [real-estate-ads-list-url]`

Warning: you have to manually enter the city in the database before crawling.

### Loan simulator

The loan simulator is usable. All you have to do is fill the simulator form...
That's all.

## Todo

### Next developments

- Add unit tests on the calculator feature
- Automatically add city while crawling
- Add real estate ad page

### Some ideas (to do next)

- Add support of other real estate ads websites
- Add a better crawling system
- A custom user search that will generate urls for real estate ads websites and crawl them
- Crawl real estate ad's photos
