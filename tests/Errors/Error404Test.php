<?php

namespace Tests\AppBundle\Controller;

use Oks\Component\Test\WebTestCaseWithTranslator;

class Error404Test extends WebTestCaseWithTranslator
{
    /**
     * Test homepage.
     */
    public function testIndex()
    {
        // Initialize
        $client = static::createClient(array(
            'debug'       => false,
        ));
        $crawler = $client->request('GET', '/404');

        // Initialize response information
        $webResponse = $client->getResponse();
        $statusCode = $webResponse->getStatusCode();
        $statusText = $webResponse::$statusTexts[$statusCode];

        // Check status
        $this->assertEquals(404, $statusCode);

        // Check contents
        $this->assertContains(
            'Oikos',
            $crawler->filter('.centered-content-body h1')->text()
        );
        $this->assertContains(
            self::$translator->trans('Error.Title', [], 'Error'),
            $crawler->filter('.centered-form h2')->text()
        );
        $this->assertContains(
            self::$translator->trans('Error.Details', [
                '%status_code%' => $statusCode,
                '%status_text%' => $statusText
            ], 'Error'),
            $crawler->filter('.centered-form p.error')->text()
        );
        $this->assertContains(
            self::$translator->trans('Error.MainText', [], 'Error'),
            $crawler->filter('.centered-form p')->last()->text()
        );
    }
}
