<?php

namespace Tests\UserBundle\Controller;

use Oks\Component\Test\WebTestCaseWithTranslator;

class ResettingControllerTest extends WebTestCaseWithTranslator
{
    /**
     * Test homepage.
     */
    public function testIndex()
    {
        // Initialize
        $client = static::createClient();
        $crawler = $client->request('GET', '/resetting/request');

        // Check status
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Check page title
        $this->assertContains(
            self::$translator->trans('layout.forgotten_password', [], 'FOSUserBundle'),
            $crawler->filter('.centered-form h2')->text()
        );

        // Check that we have a link to the login page
        $this->assertContains(
            self::$translator->trans('layout.login', [], 'FOSUserBundle'),
            $crawler->filter('.centered-footer .action-btn')->text()
        );
    }
}
