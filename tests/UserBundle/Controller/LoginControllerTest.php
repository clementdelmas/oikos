<?php

namespace Tests\UserBundle\Controller;

use Oks\Component\Test\WebTestCaseWithTranslator;

class LoginControllerTest extends WebTestCaseWithTranslator
{
    /**
     * Test homepage.
     */
    public function testIndex()
    {
        // Initialize
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        // Check status
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Check page title
        $this->assertContains(
            self::$translator->trans('layout.login', [], 'FOSUserBundle'),
            $crawler->filter('.centered-content-body h2')->text()
        );

        // Check that we have a link to the forgotten password page
        $this->assertContains(
            self::$translator->trans('security.login.lost_password', [], 'FOSUserBundle'),
            $crawler->filter('.centered-form .forgotten-password-link')->text()
        );

        // Check that we have a link to the register page
        $this->assertContains(
            self::$translator->trans('layout.register', [], 'FOSUserBundle'),
            $crawler->filter('.centered-footer .action-btn')->text()
        );
    }
}
