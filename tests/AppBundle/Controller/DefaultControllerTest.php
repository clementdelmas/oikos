<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * Test homepage.
     */
    public function testIndex()
    {
        // Initialize
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        // Check status
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Oikos', $crawler->filter('.wrapper h1')->text());
    }
}
