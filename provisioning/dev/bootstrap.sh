#!/usr/bin/env bash

# Initialize
source ~/.profile

# Initialize: go to project folder
cd $PATH_TO_PROJECT_FOLDER
echo "In the ${PATH_TO_PROJECT_FOLDER} folder"

# Install composer dependencies
composer install --prefer-source --no-interaction

# Fix permissions
chmod -R 777 $PROJECT_CACHE_FOLDER $PROJECT_LOGS_FOLDER

# Install NPM and Bower modules
npm install --save-dev

# Execute gulp in production environment
# (compile assets and don't watch sass folders)
gulp --production
