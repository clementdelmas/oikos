#!/usr/bin/env bash

# Initialize
project_folder='/var/www/html/oikos'
cache_dir="${project_folder}/var/cache"
logs_dir="${project_folder}/var/logs"
vhost_file="${project_folder}/provisioning/dev/vhost.conf"

# Use single quotes instead of double quotes to make it work with special-character passwords
user_database_password='oikos'

# AMP configuration
php_date_timezone='Europe/Paris'

# Export variables
if [ -z "$PATH_TO_PROJECT_FOLDER" ] || [ "$PATH_TO_PROJECT_FOLDER" != "${project_folder}" ]; then
  exec_cmd="export PATH_TO_PROJECT_FOLDER=${project_folder}"
  $exec_cmd && echo "$exec_cmd" >> ~/.profile
fi
if [ -z "$PROJECT_CACHE_FOLDER" ] || [ "$PROJECT_CACHE_FOLDER" != "${cache_dir}" ]; then
  exec_cmd="export PROJECT_CACHE_FOLDER=${cache_dir}"
  $exec_cmd && echo "$exec_cmd" >> ~/.profile
fi
if [ -z "$PROJECT_LOGS_FOLDER" ] || [ "$PROJECT_LOGS_FOLDER" != "${logs_dir}" ]; then
  echo "export PROJECT_LOGS_FOLDER=${logs_dir}" >> ~/.profile
	$exec_cmd && echo "$exec_cmd" >> ~/.profile
fi
if [ -z "$PROJECT_VHOST_FILE" ] || [ "$PROJECT_VHOST_FILE" != "${vhost_file}" ]; then
  echo "export PROJECT_VHOST_FILE=${vhost_file}" >> ~/.profile
	$exec_cmd && echo "$exec_cmd" >> ~/.profile
fi
if [ -z "$USER_DB_PASSWORD" ] || [ "$USER_DB_PASSWORD" != "${user_database_password}" ]; then
  echo "export USER_DB_PASSWORD=${user_database_password}" >> ~/.profile
	$exec_cmd && echo "$exec_cmd" >> ~/.profile
fi
if [ -z "$DATE_TIMEZONE" ] || [ "$DATE_TIMEZONE" != "${php_date_timezone}" ]; then
  echo "export DATE_TIMEZONE=${php_date_timezone}" >> ~/.profile
	$exec_cmd && echo "$exec_cmd" >> ~/.profile
fi
