#!/usr/bin/env bash

# Initialize
source ~/.profile

# Create project folder
sudo mkdir -p "$FULL_PATH_TO_PROJECT_FOLDER"

# Install apache 2.5 and php 5.5
sudo apt-get install -y apache2
sudo apt-get install -y php5

# Install PHP modules
sudo apt-get install php5-apcu
sudo apt-get install php5-curl
# sudo apt-get install php5-geoip
sudo apt-get install php5-imagick
sudo apt-get install php5-intl
sudo apt-get install php5-memcache

# Enable PHP modules
sudo php5enmod mcrypt

# Install mysql and give password to installer
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $USER_DB_PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $USER_DB_PASSWORD"
sudo apt-get -y install mysql-server
sudo apt-get install php5-mysql

# Install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $USER_DB_PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $USER_DB_PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $USER_DB_PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin

# Enable mod_rewrite
sudo a2enmod rewrite

# Setup hosts file
# Get and update vhost config
vhost=$(<$PROJECT_VHOST_FILE)
vhost=$(echo "$vhost" | sed -e "s/\${full\_path\_to\_project\_folder}/${FULL_PATH_TO_PROJECT_FOLDER//\//\\/}/g")
echo "$vhost" > /etc/apache2/sites-available/000-default.conf

# Set PHP timezone
sudo sed -i -e "s/\;date\.timezone =/date.timezone = \"${DATE_TIMEZONE//\//\\/}\"/g" /etc/php5/cli/php.ini
sudo sed -i -e "s/\;date\.timezone =/date.timezone = \"${DATE_TIMEZONE//\//\\/}\"/g" /etc/php5/apache2/php.ini

# Restart apache
service apache2 restart

# Install Composer
composer_hash='e115a8dc7871f15d853148a7fbac7da27d6c0030b848d9b3dc09e2a0388afed865e6a3d6b3c0fad45c48e2b5fc1196ae'
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '${composer_hash}') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
chmod 755 /usr/local/bin/composer
