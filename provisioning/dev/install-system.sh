#!/usr/bin/env bash

# Initialize
source ~/.profile

# Add NodeJS sources
curl -sL https://deb.nodesource.com/setup_4.x | sudo bash -

# Update / upgrade
sudo apt-get update
sudo apt-get -y upgrade

# Install Unix commands
sudo apt-get install mlocate
sudo updatedb

# Install GIT
sudo apt-get -y install git

# Install Node.js and NPM
sudo apt-get -y install nodejs
