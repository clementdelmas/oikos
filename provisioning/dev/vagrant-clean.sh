#!/bin/bash

# Initialize
source ~/.profile

# Clean cache and logs
rm -rf "${PROJECT_CACHE_FOLDER}" "${PROJECT_LOGS_FOLDER}"

# Recreate cache and logs folders
mkdir "${PROJECT_CACHE_FOLDER}" "${PROJECT_LOGS_FOLDER}"

# Mount cache and logs folders
mount -t tmpfs -o size=100m tmpfs "${PROJECT_CACHE_FOLDER}"
mount -t tmpfs -o  size=50m tmpfs "${PROJECT_LOGS_FOLDER}"
