#!/usr/bin/env sh

# Do clear all caches
php bin/console cache:clear
php bin/console cache:clear --env=prod
