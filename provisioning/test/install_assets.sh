#!/usr/bin/env sh

# Install NPM packages
npm install --save-dev

# Rebuild required for CodeShip
npm rebuild node-sass

# Do install bower packages
bower install

# Execute gulp in production environment
# (compile assets and don't watch sass folders)
gulp --production
