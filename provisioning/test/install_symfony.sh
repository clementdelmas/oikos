#!/usr/bin/env sh

# We need to define our credentials here via environment vars as
# thats the way we get mysql credentials in Codeship.
if [ -z $SYMFONY__TEST__DATABASE_USER ]; then
    export SYMFONY__TEST__DATABASE_USER=oikos
fi
if [ -z $SYMFONY__TEST__DATABASE_PASSWORD ]; then
    export SYMFONY__TEST__DATABASE_PASSWORD=oikos-pass
fi
if [ -z $SYMFONY__TEST__DATABASE_NAME ]; then
    export SYMFONY__TEST__DATABASE_NAME=oikos
fi

# Check for Shippable
if [ "shippable" = "$SYMFONY__TEST_CI_NAME" ]; then
    # Reset database password for Shippable
    export SYMFONY__TEST__DATABASE_PASSWORD=
fi

# Do composer install
composer install --prefer-source --no-interaction

# Check for Shippable
if [ "shippable" = "$SYMFONY__TEST_CI_NAME" ]; then
  # Update parameters.yml with test database credentials
  echo "    test_database_name: $SYMFONY__TEST__DATABASE_NAME" >> ./app/config/parameters.yml
  echo "    test_database_user: $SYMFONY__TEST__DATABASE_USER" >> ./app/config/parameters.yml
  echo "    test_database_password: $SYMFONY__TEST__DATABASE_PASSWORD" >> ./app/config/parameters.yml

  # Clear cache
  php ./bin/console --env=test cache:clear

  echo "[ DEBUG parameters.yml ]"
  cat ./app/config/parameters.yml
  echo "[ END DEBUG ]"
fi

# Fix symfony permissions
# @see http://symfony.com/doc/current/setup/file_permissions.html
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var

# Setup and migrate DB and clear cache
# @see http://documentation-symfony.fr/bundles/DoctrineMigrationsBundle/index.html
# php ./bin/console --env=test doctrine:database:drop --force
php ./bin/console --env=test doctrine:database:create
php ./bin/console --env=test doctrine:schema:update --force
php ./bin/console --env=test doctrine:migrations:migrate --no-interaction
php ./bin/console --env=test cache:clear
