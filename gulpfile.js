/* globals __dirname: true */
// Requirements
var gulp            = require('gulp'),

// Custom node modules
    gulpif          = require('gulp-if'),
    argv            = require('yargs').argv,

// SASS and CSS requirements
    sass            = require('gulp-sass'),
    concat          = require('gulp-concat'),
    notify          = require('gulp-notify') ,
    cleanCSS        = require('gulp-clean-css'),
    autoprefixer    = require('gulp-autoprefixer'),

// Javascript requirements
    jshint          = require('gulp-jshint'),
    filesize        = require('gulp-filesize'),
    uglify          = require('gulp-uglify')
;

// Custom console colors
require('colors');

// Initialize environment
var isProductionEnv = (argv.production !== undefined);

// Initialize config
var config          = {
    sassDir:        './app/Resources/assets/scss',
    fontsDir:       './app/Resources/assets/fonts',
    jsDir:          './app/Resources/assets/js',
    bowerDir:       './bower_components',
    webDir:         './web',
};

var watchDirs = [
  config.sassDir + '/*.scss',
  config.sassDir + '/**/*.scss',
  './src/Oks/Bundle/*Bundle/Resources/assets/**/*.scss',
  './src/Oks/Bundle/*Bundle/Resources/assets/**/**/.scss',
];

/** CSS Task **/
gulp.task('sass', function() {
  gulp.src(config.sassDir + '/main.scss')
      .pipe(sass({
        includePaths:   [
          config.bowerDir + '/bootstrap-sass/assets/stylesheets',
          config.bowerDir + '/fontawesome/scss',
          config.bowerDir + '/awesome-bootstrap-checkbox',
          config.sassDir
        ],
        outputStyle: (isProductionEnv ? 'compressed' : 'compact')
      }))
      .on('error', sass.logError)
      .pipe(gulpif(isProductionEnv, cleanCSS({ compatibility: 'ie8' })))
      .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
      .pipe(concat('style.min.css'))
      .pipe(gulp.dest(config.webDir + '/css'))
      .pipe(filesize())
    ;
});

gulp.task('watch', function() {
  var runTasks = [ 'sass' ];
  return gulp
    // Watch the input folder for change, and run `sass` task when something happens
    .watch(watchDirs, runTasks)
    // When there is a change, log a message in the console
    .on('change', function(ev) {
        // Initialize
        var message = 'File `'.gray + ev.path.replace(__dirname, '.').green + '` was '.grey + ev.type.yellow +
                      ', running '.gray + runTasks.join(', ').cyan + (' task' + (runTasks.length > 1 ? 's' : '') + '.').gray;

        // Log message in console and notify message with growl
        console.log(message);
        notify(message);
    });
});


/** JS Hint Task **/
gulp.task('lint', function() {
  return gulp.src('./app/assets/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

/** Javascript Task **/
gulp.task('js', function () {
    gulp.src([
      config.jsDir + '/vendor/common/*.js',
      config.jsDir + '/vendor/bootstrap/*.js',
      config.jsDir + '/vendor/plugins/*.js',
      config.jsDir + '/vendor/paper/*.js'
      // config.jsDir + '/oikos/*.js'
    ])
      .pipe(gulpif(isProductionEnv, uglify())) // only minify in production
      .pipe(concat('main.min.js'))
      .pipe(gulp.dest(config.webDir + '/js'))
      .pipe(filesize())
    ;
});

/**
 *  Load fonts
 */
gulp.task('fonts', function() {
  gulp.src([
    config.bowerDir + '/fontawesome/fonts/*',
    config.bowerDir + '/bootstrap-sass/assets/fonts/bootstrap/*',
    config.fontsDir + '/*'
  ])
      .pipe(gulp.dest(config.webDir + '/fonts'))
      // .pipe(filesize())
  ;
});

// Initialize tasks to do
var tasksToDo = ['sass', 'fonts', 'lint', 'js'];

// Check production environment
if (!isProductionEnv) {
  // If we are not in production,
  // let's do some other tasks
  tasksToDo.push('watch');
}

// Let's do this !
gulp.task('default', tasksToDo);
