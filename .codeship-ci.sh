#!/usr/bin/env sh
#
# @see https://github.com/akamalov/slackmessenger/blob/master/codeship-steps.yml
# @see https://codeship.com/documentation/docker-integration/php/
# @see https://codeship.com/documentation/docker/steps/
#
# Set PHP environment
phpenv local 7.1
export SYMFONY_ENV=test

# Set NodeJs environment
nvm install 4.4.5
nvm use 4.4.5

# Update INI files
echo "xdebug.max_nesting_level = 250" >> $HOME/.phpenv/versions/5.6/etc/php.ini
echo "memory_limit = 512M" >> $HOME/.phpenv/versions/5.6/etc/php.ini

# Initialize database
db_name="test_oikos"

# Create test database and load fixtures
# mysql -u $MYSQL_USER -p $MYSQL_PASSWORD -e "CREATE DATABASE $db_name"
# unzip -p app/Resources/Tests/test.sql.zip | mysql -u $MYSQL_USER -p $MYSQL_PASSWORD $db_name

# Update configuration parameters
cp app/config/parameters.yml.dist app/config/parameters.yml
sed -i "s/database_name:.*/database_name: $db_name/"                ./app/config/parameters.yml
sed -i "s/database_user:.*/database_user: $MYSQL_USER/"             ./app/config/parameters.yml
sed -i "s/database_password:.*/database_password: $MYSQL_PASSWORD/" ./app/config/parameters.yml

# Fix MySQL parameters
export SYMFONY__TEST_DATABASE_USER=$MYSQL_USER
export SYMFONY__TEST_DATABASE_PASSWORD=$MYSQL_PASSWORD
export SYMFONY__TEST_DATABASE_NAME=$db_name

# Do install Symfony and Assets
./provisioning/test/install_symfony.sh
./provisioning/test/install_assets.sh
